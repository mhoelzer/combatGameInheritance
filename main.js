// want all the instances and calls here 
const hero = new HeroCharacter({
    name: prompt("Enter your name here:"),
    health: 250,
    chanceForMissing: .3,
    chanceForSuperMaxDamage: .05
});
const orangeFurby = new Character({
    name: `Orange "El Jefe" Furby`,
    health: 100,
    normalDamage: 20,
    chanceForMaxDamage: .7,
    chanceForMissing: .65,
});
const magentaFurby = new Character({
    name: "Magenta Furby",
    chanceForMissing: .3
});
const tigerFurby = new Character({
    name: "Tiger Furby",
    health: 65
});

hero.backgroundInfo();
hero.deathMatch(magentaFurby, tigerFurby, orangeFurby);