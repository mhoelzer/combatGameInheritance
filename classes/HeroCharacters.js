// put the constructor stuff here, like for characters
function HeroCharacter(characteristics) {
    Character.call(this, characteristics);
    this.chanceForSuperMaxDamage = characteristics.chanceForSuperMaxDamage;
};
// HeroCharacter.prototype = new Character()
HeroCharacter.prototype = Object.create(Character.prototype)
HeroCharacter.prototype.constructor = HeroCharacter